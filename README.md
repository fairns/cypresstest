### How to use

Run an `npm install` from the main directory in order to download the required plugins
Run `npx cypress run` from the main directory in order to run the tests
Run `npx cypress open` from the main directory in order to view the tests in their app

Run the examples from there